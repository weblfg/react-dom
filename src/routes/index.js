import List from '../views/admin/products/List'
import index from '../views/admin/dashboard'
import PageNotFound from '../views/error/PageNotFound'
import Login from '../views/Login/Login'
import Edit from '../views/admin/products/Edit'
import {TwitterOutlined,WechatOutlined} from '@ant-design/icons'
// 路由一个是主要的路由，一个是需要验证的路由
export const mainRoutes = [{
    path: '/Login',
    component:Login
},{
    path:'/404',
    component:PageNotFound
}]
export const adminRoutes = [{
    path: '/admin/dashboard',
    component:index,
    isShow:true,
    title:'看板',
    icon:<TwitterOutlined />
}, {
    path: '/admin/products',
    component: List,
    exact: true, //精确匹配
    isShow:true,
    title:'商品管理',
    icon:<WechatOutlined />
}, {
    path: '/admin/products/Edit/:id',
    component:Edit,
    exact:true,
    isShow:false
}]
