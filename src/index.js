import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import {Provider} from 'react-redux'
import 'antd/dist/antd.css';
import './index.scss';
import App from './App';
import { mainRoutes } from './routes';
import store from './store/store';

ReactDOM.render(
  <Provider store={store}>
    <Router>
    <Switch>
    <Route path='/admin' render={routeProps=><App {...routeProps}/>}/>
      {mainRoutes.map(route => {
        return <Route key={route.path} {...route}></Route>
      })}
      <Redirect to='/admin' from='/' />
      <Redirect to='/404'></Redirect>
    </Switch>
  </Router>
  </Provider>
  ,
  document.getElementById('root')
);


