import React, { Component } from 'react';
import { Form, Input, Button, message } from 'antd';
import { get } from '../../utils/request'
class Login extends Component {
    async getToken() {
        let data = await get('1.json')
        if (data.status === 200) {
            sessionStorage.setItem('token', data.data.token)
            this.props.history.push('/admin/products/1')
        }
    }
    render() {
        const onFinish = (values) => {
            console.log('Success:', values);
            if (!!values.username && !!values.passwd) {
                if (values.username === '张三' && values.passwd === "123") {
                    this.getToken()
                }else{
                    message.error('用户名或者密码错误')
                }
            }
            
        };
        const onFinishFailed = (errorInfo) => {
            console.log('Failed:', errorInfo);
        };
        return (
            <React.Fragment>
                <div className='LoginPage'>
                    <Form
                        className='LoginForm'
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed} >
                        <div className='loginIn'>
                            登录
                        </div>
                        <div>
                            <Form.Item label='姓名' name='username'>
                                <Input placeholder='张三' />
                            </Form.Item>
                            <Form.Item label='密码' name='passwd'>
                                <Input placeholder='123' />
                            </Form.Item>
                        </div>
                        <Form.Item>
                            <Button htmlType='submit' type='primary'>Login</Button>
                        </Form.Item>
                    </Form>
                </div>
            </React.Fragment>
        )
    }
}
export default Login;