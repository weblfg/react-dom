import store from '../../../store/store'
import { Form, Card, Input, Row, Col,Button,message } from 'antd'
const onFinish = (values) => {
    store.dispatch({type:'add',result:values})
    message.success('保存成功')
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
function Edit() {
    return (
        <Card title='商品编辑'>
            <Form 
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
       
            >
                <Row justify="space-between">
                    <Col span={11}>
                        <Form.Item label='姓名' name='name' rules={[{ required: true, message: '请输入用户名!' }]}>
                            <Input placeholder='输入姓名' />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label='年龄' name='age' rules={[{ required: true, message: '请输入密码!' }]}>
                            <Input placeholder='输入密码' />
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item>
                   <Row justify='center'>
                       <Col span={8}>
                       <Button type='primary'  htmlType="submit" style={{width:'100%'}}>保存</Button>
                       </Col>
                   </Row>
                </Form.Item>
            </Form>
        </Card>
    )
}
export default Edit
