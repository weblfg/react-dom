import store from '../../../store/store'
import { Button, Card, Table, message } from 'antd'
import { Component } from 'react'
const columns = [{
    title:'序号',
    key:'id',
    dataIndex:'id',
    render:(value,row,index)=>index+1
},{
    id:1,
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
}, {
    id:2,
    title: '年龄',
    dataIndex: 'age',
    key: 'age'
}, {
    title: '操作',
    render: (value, row, index) => {
        return (
            <div>
                 <Button size='small' onClick={()=>Confirm(row)} danger >删除</Button>
                <Button style={{ margin: "0 1rem" }} size='small' type="primary" ghost > 修改</Button>
            </div>
        )
    }
}]

function Confirm(row) {
    store.dispatch({type:'del',result:row})
    return message.success('删除成功')
}

class List extends Component {
    constructor(props){
        super(props)
        this.data=store.getState()
    }
    componentDidMount(){
        store.subscribe(()=>{
            this.forceUpdate()
        })
    }
    render(){
        return (
            <Card title='商品列表' extra={<Button type='primary' onClick={()=>this.props.history.push('/admin/products/Edit/1')}>新增</Button>}>
                <Table dataSource={[...this.data]} columns={columns}  rowKey={record =>record.name} />
            </Card>
        )
    }
}

export default List
