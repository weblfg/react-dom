import { createStore } from 'redux'
function notices(state = [{
    id: 1,
    name: '张三',
    age: 22
}, {
    id: 2,
    name: '小明',
    age: 33
}], action) {
    switch (action.type) {
        case 'add':
            state.push(action.result)
            return state;

        case 'del':
            state.forEach((item, index) => {
                if (item.name === action.result.name) {
                    state.splice(index, 1)
                }
            })
            debugger
            return state

        default: return state

    }
}
const store = createStore(notices)
export default store;