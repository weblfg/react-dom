import axios from 'axios'
import { getToken } from './auth'
axios.create({
    timeout: 3000,
    baseURL: 'http://localhost:3000'
})
export function get(url) {
    return axios.get(url)
}
export function post(url, data) {
    return axios.post(url, data)
}
export function put(url, data) {
    return axios.put(url, data)
}
axios.interceptors.request.use(
    config => {
       
        const token = getToken()
        token && (config.headers.Authorization = token);
        return config
    }
    ,
    error => {
        return error
    }
)
