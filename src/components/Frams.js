import { Layout, Menu } from 'antd';
import { adminRoutes } from '../routes';
import {withRouter} from 'react-router-dom'
const { Header, Content, Sider } = Layout;
const routes=adminRoutes.filter(item=>item.isShow)
function Frams(props) {
    return (
        <>
            <Layout className='layout'>
                <Header className="header">
                    <div className="logo" />
                    <div className='title'>React demo</div>
                </Header>
                <Layout>
                    <Sider width={200} className="site-layout-background">
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            style={{ height: '100%', borderRight: 0 }}
                        >
                            {routes.map(route=>{
                               return <Menu.Item key={route.path} onClick={p=>props.history.push(p.key)}>
                                  <i>{route.icon}</i>
                                   {route.title}</Menu.Item>
                            })}
                          
                        </Menu>
                    </Sider>
                    <Layout style={{ padding: '0 24px 24px' }}>
                        <Content
                            className="site-layout-background"
                            style={{
                                padding: 24,
                                margin: 0,
                                minHeight: 280,
                            }}
                        >
                            {props.children}
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        </>
    )
}

export default withRouter(Frams)
