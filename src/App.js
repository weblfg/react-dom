import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { adminRoutes } from './routes'
import Frame from './components/Frams'
import {isLogin} from './utils/auth'
import './App.scss'
function App() {
  if(isLogin()){
    return  (
      <div className='App'>
       { <Frame>
        <Switch>
          {adminRoutes.map(route=>{
            return <Route key={route.path} exact={route.exact} path={route.path} render={routerProps=>{
              return <route.component {...routerProps} />
            }}/>
          })}
           <Redirect to={adminRoutes[1].path} from='/admin' />
          <Redirect to='/404' />
        </Switch>
        </Frame>}
      </div>
    )
  }else{
    return (<Redirect to='/login'/>)
  }
  
}

export default App

